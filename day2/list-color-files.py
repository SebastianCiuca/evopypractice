import os
import sys
from colors import *


def human_size(b):
    if b<1024:
        return str(b) + " b"
    elif b<1024*1024:
        return str(b//1024) + " kb"
    else:
        return str(b//1024*1024) + " mb"


def main():
    if (len(sys.argv)) > 1 and os.path.isdir(sys.argv[1]):
        path = str(sys.argv[1])
        colorful_list(path)
    else:
        print "A directory has not been passed!"



def parse_files(files,path):
    maxlen = 0
    res = []
    for f in files:
        if not os.path.isdir(f):
            filesize = os.path.getsize(os.path.join(path, f))
            if len(f) > maxlen:
                maxlen = len(f)
            type = f.split(".")[1]
            tup = (f, filesize, type)
            res.append(tup)

    return res,maxlen




def parse_dirs(files,path):
    maxlen = 0
    res = []
    for f in files:
        if os.path.isdir(f):
            count_files = len(os.listdir(os.path.join(path,f)))
            if len(f)>maxlen:
                maxlen = len(f)
            tup = (f,count_files)
            res.append(tup)

    return res,maxlen

def print_files(file_print,gmax):
    for f in file_print:
        if f[2] == "py":
            print colored_text(str(f[0])+ " " * (gmax - len(f[0])) + "\t" +human_size(f[1]) + "\t" + str(f[2]),LGREEN)
        else:
            print str(f[0])+ " " * (gmax - len(f[0])) + "\t" + human_size(f[1]) + "\t" + str(f[2])

def print_dir(dir_print,gmax):
    for d in dir_print:
        print colored_text(str(d[0])+ " " * (gmax - len(d[0])) +"\t" +str(d[1]) + "\titems",DBLUE)

def colorful_list(path):
    files = os.listdir(path)
    file_print, dir_print = [],[]
    maxlen = maxlen2 = 0          #lungime maxima coloana nume fisier / director

    #separate files in cd into 2 lists: files and dirs
    file_print, maxlen = parse_files(files,path)
    dir_print, maxlen2 = parse_dirs(files,path)

    #sort the lists
    file_print.sort(key = lambda  x: x[0])
    dir_print.sort(key = lambda x: x[0])

    #get the maximum name length
    gmax = max(maxlen,maxlen2)

    #print them
    print_dir(dir_print, gmax)
    print_files(file_print,gmax)


main()



