"""
Day of the week. 
Write a program that takes a date as input and prints the day of 
the week that date falls on. 
Your program should take three command-line arguments: m (month), d (day), and y (year). 
For m use 1 for January, 2 for February, and so forth. 
For output print 0 for Sunday, 1 for Monday, 2 for Tuesday, and so forth. 
Use the following formulas, for the Gregorian calendar (where / denotes integer division):

y0 = y - (14 - m) / 12
x = y0 + y0/4 - y0/100 + y0/400
m0 = m + 12x((14-m)/12)-2
d0 = (d + x + 31m0 / 12) mod 7

For example, on which day of the week was August 2, 1953?
"""

def calendar(m,d,y):
    #days = [i for i in range(0,8)]
    actualDays = ['Sunday','Monday','Tuesday','Wensday','Thursday','Friday','Saturday']
    months = [i for i in range(1,13)]

    #mapare (lista nume) -> (lista numere): numere[nume.index(obj)]

    y0 = y - (14 - m) // 12
    x = y0 + y0//4 - y0//100 +y0 //400
    m0 = m + 12*x * ((14-m)//12) -2
    d0 = (d+x+31*m0 // 12) % 7

    print "Gregorian calendar: day~" + str(actualDays[d0]) + ", month~" + str(months[m0]) + ", year~" + str(y0)

calendar(8,2,1953)