# this function should return a tuple
# the first element is a list of integers representing the isbn
# the second element is the check digit
# parse_isbn('ISBN817525766-0') == ([8, 1, 7, 5, 2, 5, 7, 6, 6], 0)

def parse_isbn(isbn):
    isbn_check = int(isbn[-1])
    isbn_main = [int(i) for i in isbn[4:-2]]
    return isbn_main, isbn_check

def is_isbn_valid(isbn):
    checksum = 0
    # j = ( [a b c d e f g h i] * [1 2 3 4 5 6 7 8 9] ) mod 11
    main,check = parse_isbn(isbn)

    for i in range(1,len(main)+1):
        main[i-1] *= i

    for i in main:
        checksum += i

    return checksum % 11 == check #0

	# a = isnb_numbers
	# nr = range(9,0,-1)
	# zip (a,nr) -> returneaza perechi (a[i],nr[i])
	# for el in zipres: rez = el[0] * el[1]
	# mylist = [x*i for x,i in zip(a,nr)] ; sum(mylist)

# py.test code below

def test_parse_isbn():
    assert parse_isbn('ISBN817525766-0') == ([8, 1, 7, 5, 2, 5, 7, 6, 6], 0)

def test_is_isbn_valid():
    assert is_isbn_valid('ISBN817525766-0')
    assert not is_isbn_valid('ISBN817525767-0')
