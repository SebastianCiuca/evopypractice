import os
import json
import textwrap
import argparse
import sys
from datetime import datetime, timedelta
from colors import *
from commands import *

def main():

    if len(sys.argv) == 1:
        print "Invalid parameters! \n Commands: {list,add,done}"
        return


    if (str(sys.argv[1]) == "add" or str(sys.argv[1]) == "done") and len(sys.argv) < 3:
        print "Invalid nb of parameters! 'add' requires a task and 'done' requires an id."
        return

    elif str(sys.argv[1]) == "list":
        if len(sys.argv) < 3:
            command_list_file()
        elif sys.argv[2] == '--file':
            command_list_file()
        elif sys.argv[2] == '--db':
            command_list_db()
        else:
            print "Invalid persistancy flag! \n\t{--file,--db}"

    elif str(sys.argv[1]) == "add":
        if len(sys.argv) < 4:
            command_add_file(str(sys.argv[2]))
        elif sys.argv[3] == '--file':
            command_add_file(sys.argv[2])
        elif sys.argv[3] == '--db':
            command_add_db(sys.argv[2])
        else:
            print "Invalid persistancy flag! \n\t{--file,--db}"

    elif str(sys.argv[1]) == "done":
        if len(sys.argv) < 3:
            command_done(str(sys.argv[2]))
        elif sys.argv[3] == '--file':
            command_done_file(sys.argv[2])
        elif sys.argv[3] == '--db':
            command_done_db(sys.argv[2])
        else:
            print "Invalid persistancy flag! \n\t{--file,--db}"

main()
