class Todo(object):
    def __init__(self, message, id=None, done=False, created=None):
        self.message = message
        self.id = id
        self.done = done
        self.created = created
