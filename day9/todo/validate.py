def validate_int(nb):
    try:
        id = int(nb)
	return id
    except ValueError:
        raise ValueError("Cannot convert current number.")

def validate_line(line):
    task = line.split(",")

    try:
        validate_int(task[1])
    except ValueError:
        raise ValueError("Cannot convert current id.")

    try:
        validate_int(task[2])
    except ValueError:
        raise ValueError("Cannot convert current task done status.")

    data = task[3].split("/")

    if len(data) != 6:
        raise ValueError("Invalid date given!")

    for d in date:
        try:
            validate_int(d)
        except ValueError:
            raise ValueError("Invalid date given!")
