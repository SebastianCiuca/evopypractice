from file_utils import *
from json_utils import *
from domain import *
from datetime import datetime
from todo_sql import *
import validate

def command_add_file(t):
    '''
    Add command for File Repo.
    '''

    frepo = FileRepo("tasks.txt")

    #create new To do object
    newtask = Todo(t,frepo.get_id(),False,datetime.datetime.now())

    #add in repo
    try:
        frepo.add_task(newtask)
    except IOError as e:
        print e.strerror


def command_add_db(t):
    """
        Add for DB Repo.
    """

    dbrepo = SQLRepo("dbtodo","tasks")

    newtask = Todo(t, dbrepo.get_id(), False, datetime.datetime.now())

    try:
        dbrepo.add_task(newtask)
    except sqlite3.Error as e:
        print e.strerror


def command_done_file(id):
    '''
     Done command for File Repo.
    '''

    try:
        id = validate_int(id)
    except ValueError as e:
        print e.strerror

    frepo = FileRepo("tasks.txt")

    try:
        tasks = frepo.parse_file()

        for t in tasks:
            if t.id == id:
                t.done = True

        frepo.populate_file(tasks)

    except IOError as e:
        print e.strerror


def command_done_db(id):
    """
        Done command for DB Repo.
    """
    try:
        id = validate_int(id)
    except ValueError as e:
        print e.strerror

    dbrepo = SQLRepo("dbtodo", "tasks")

    try:
        dbrepo.done_task(id)
    except sqlite3.Error as e:
        print e.strerror



def command_list_file():
    '''
        List command for File Repo.
    '''

    frepo = FileRepo("tasks.txt")


    try:
        tasks = frepo.parse_file()

        for t in tasks:
            if t.done == False:
                print parse_date(t)

    except IOError as e:
        print e.strerror


def command_list_db():
    """
        List command for DB Repo.
    """
    dbrepo = SQLRepo("dbtodo", "tasks")

    try:
        tasks = dbrepo.get_all()

        for t in tasks:
            if t.done == False:
                print parse_date(t)

    except sqlite3.Error as e:
        print e.strerror


def human_time(td):

    if td.days > 1:
        return str(td.days) + " days ago"

    if td.seconds > 3600:
        return str(td.seconds // 3600) + " hours ago"

    if td.seconds > 60:
        return str(td.seconds // 60) + " minutes ago"

    return str(td.seconds) + " seconds ago"


def parse_date(task):

    current_time = datetime.datetime.now()
    task_time = task.created

    dif = datetime.datetime.strptime(str(current_time),'%Y-%m-%d %H:%M:%S.%f') - \
          datetime.datetime.strptime(str(task_time),'%Y-%m-%d %H:%M:%S.%f')
    ago = human_time(dif)

    return "id " + str(task.id) + "\t to do \t " + ago + " \n\t" + str(task.message)