import json
from datetime import date
from domain import *

#encode
def to_json(obj1):

    obj = {}
    obj["message"] = obj1.message
    obj["id"] = str(obj1.id)
    obj["done"] = str(obj1.done)
    obj["created"] = str(obj1.created)

    return json.dumps(obj)

#decode
def from_json(linie_fisier):

    t = json.loads(linie_fisier)

    if t["done"] == "True":
        tobj = Todo(t["message"],t["id"],True,t["created"])
    else:
        tobj = Todo(t["message"],t["id"],False,t["created"])

    return tobj

