import sqlite3
from domain import *
import datetime


class SQLRepo(object):

    def __init__(self,dbname,tablename):
        self.dbname = dbname
        self.tname = tablename

    def create_table(self):
        self.dbname.strip('.db')

        conn = sqlite3.connect(self.dbname + '.db')

        c = conn.cursor()

        c.execute('CREATE TABLE IF NOT EXISTS ' + self.tname + ' (task text, id int, done tinyint, created datetime)')

        conn.commit()

        c.close()


    def get_all(self):

        self.create_table()

        self.dbname.strip('.db')

        conn = sqlite3.connect(self.dbname + '.db')

        c = conn.cursor()

        c.execute('SELECT * FROM '+self.tname)

        item_list = c.fetchall()

        conn.close()

        return [self.touple_to_task(item) for item in item_list]

    def add_task(self,atask):

        self.create_table()


        self.dbname.strip('.db')

        conn = sqlite3.connect(self.dbname + '.db')

        c = conn.cursor()

        message, id, done, created = atask.message, atask.id, atask.done, atask.created

        c.execute('INSERT INTO ' + self.tname + ' VALUES (?,?,?,?)',[message,id,done,created])

        conn.commit()

        c.close()

    def get_id(self):
        try:
            return len(self.get_all())
        except:
            return 0

    def done_task(self,id):
        self.create_table()

        self.dbname.strip('.db')

        conn = sqlite3.connect(self.dbname + '.db')

        c = conn.cursor()

        c.execute('UPDATE ' + self.tname + ' SET done = 1 WHERE id = ' + str(id))

        conn.commit()

        c.close()

    @staticmethod
    def touple_to_task(task):
        return Todo(task[0],task[1],task[2],task[3])


def test_dbpersistancy():
    mydb = SQLRepo("dbtodo","tasks")
    #assert mydb.get_all() == [(u'Get db persistancy to work', 0, 0, u'2017-07-31 22:53:24.753823')]

    mydb.done_task(0)
    assert mydb.get_all() == [(u'Get db persistancy to work', 0, 1, u'2017-07-31 22:53:24.753823')]