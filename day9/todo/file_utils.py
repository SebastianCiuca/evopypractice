from json_utils import *


class FileRepo(object):

    def __init__(self,fname):
        self.fname = fname

    def parse_file(self):

        tasks = []

        with open(self.fname) as f:
            for l in f:
                tasks.append(from_json(l)) #lista de obiecte To do

        return tasks

    def populate_file(self):

        with open(self.fname,"w") as f:

            #delete file content
            f.seek(0)
            f.truncate()

            #populate file
            for t in self.parse_file():
                f.write(to_json(t) + "\n")

    def add_task(self,task):
        with open("tasks.txt", "a") as f:
            f.write(to_json(task) + "\n")

    def get_id(self):

        myid = 0

        with open(self.fname) as f:
            for l in f:
                myid += 1
        return myid
