import random


def srp():
    """
        Scissors, rock, paper.
    """

    #random choice([1,2,3])
    # 1 - scissors, 2 - rock, 3 - paper
    
    player1 = random.randint(1,3)
    player2 = random.randint(1,3)

    if player1 == player2:
        print "It's a draw! Both players have the same item."
        return

    if player1 == 1:
        if player2 == 2:
            print "Player 1 - scissors, Player 2 - rock => P2 wins!"
            return
        print "Player 1 - scissors, Player 2 - paper => P1 wins!"
        return

    if player1 == 2:
        if player2 == 1:
            print "Player 1 - rock, Player 2 - scissors => P1 wins!"
            return
        print "Player 1 - rock, Player 2 - paper => P2 wins!"
        return

    if player2 == 1:
        print "Player 1 - paper, Player 2 - scissors => P2 wins!"
        return
    print "Player 1 - paper, Player 2 - rock => P1 wins!"
    return

    # d = {
    #     (ROCK,PAPER): SECOND_WIN,
    #     (ROCK,SCISSORS): FIRST_WIN,
    #     (ROCK.ROCK): DRAW
    # }
    # 
    # return d[(pick1,pick2)]



    # if pick1 == pick2:
    #     return DRAW
    #
    # wins = {(ROCK,SCISSORS),(PAPER,ROCK),(SCISSORS,PAPER)}
    # return FIRST_WIN if (pick1,pick2) in wins else SECOND_WIN