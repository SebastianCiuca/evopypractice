def splits(word):
    """
    returns a list of all posible splits of word
    include the empty splits
    returns a list of tuples.
    >>> splits('ana')
    [('', 'ana'), ('a', 'na'), ('an', 'a'), ('ana', '')]
    """
    zipped = zip(word,[i for i in xrange(len(word))])
    res = []

    for t in zipped:
        res.append((word[0:t[1]],word[t[1]:len(word)]))

    res.append((word[0:t[1]+1], word[t[1]+1:len(word)]))

    return res


def deletes(word):
    """
    all misspellings of word caused by omitting a letter
    use the splits function
    >>> 'rockets' not in deletes('rocket')
    True
    >>> 'roket' in deletes('rocket')
    True
    """
    res = []
    parts = splits(word)[1:-1]

    for i in xrange(len(parts)-1):
        res.append(parts[i][0])
        res.append(parts[i][1])
        for j in xrange(i+1,len(parts)):
            res.append(parts[i][0] + parts[j][1])

    return set(res)


def inserts(word):
    """

    """

    res = []

    for i in xrange(ord('a'),ord('z')+1):
        for t in splits(word):
            res.append(t[0] + chr(i) + t[1])

    return sorted(set(res))


def transposes(word):
    """
    inverseaza 2 intre ele (2 se transpun, nu toate permutarile) => perechi
    """

def substitutions(word):
    """
    inlocuieste cate o litera;
    """

    res = []

    for i in xrange(ord('a'),ord('z')+1):
        for c in word:
            res.append(t[0] + chr(i) + t[1])

    return sorted(set(res))



#print splits("rocket")
#print deletes("rocket")

def test_splits():
    assert splits("ana") == [('', 'ana'), ('a', 'na'), ('an', 'a'), ('ana', '')]

def test_deletes():
    assert 'rockets' not in deletes('rocket')
    assert 'roket' in deletes('rocket')

print inserts("a")