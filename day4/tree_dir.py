import os
import sys
from colors import *

def main():
    #verificam daca s-a pasat un argument
    if len(sys.argv) > 1:
        level = 0
        tree_dir(str(sys.argv[1]),level)
    else:
        print "No arguments given!"


def tree_dir(path,level):

    if level == 0:
        files = os.listdir(path)
    else:
        #directoarele pot fi identificate doar cu tot path-ul aparent
        files = [os.path.join(path,f) for f in os.listdir(path)]

    for f in files:
        #daca nu e director, e fisier => printam pur si simplu
        if not os.path.isdir(f):
            lastname = f.split("/")[-1]
            print "|\t" * level + "|-> " + colored_text(lastname,DBLUE)
        else:
            #altfel printam si continuam cautarea in acest subdirector
            lastname = f.split("/")[-1]
            print "|\t" * level + "|-> " + colored_text(lastname, DRED)
            tree_dir(os.path.join(path,lastname),level+1)

main()