import random
import turtle

import time

t = turtle.Turtle()
delta = 10

def it_spiral(r):
    for r in xrange(r, 0, -delta):
        t.right(90)
        t.forward(r)

def rec_spiral(r):
    if r <= 0:
        return
    t.right(90)
    t.forward(r)
    rec_spiral(r - delta)

a = 30
s = 0.7
def tree(r):
    if r <= 15:
        return
    t.forward(r)
    t.left(a)
    tree(r*s)
    t.right(2*a)
    tree(r*s)
    t.left(a)
    t.back(r)


def draw_list(lst, highlight=0):
    t.clear()
    t.home()
    for i, a in enumerate(lst):
        if i == highlight:
            t.fillcolor('red')
        else:
            t.fillcolor('blue')
        t.begin_fill()
        t.forward(10 * a)
        t.left(90)
        t.forward(10)
        t.left(90)
        t.forward(10 * a)
        t.left(90)
        t.forward(10)
        t.end_fill()
        t.forward(15)
        t.left(90)

def koch(l, i):
    #todo
    pass

def koch_all(l, i):
    for i in xrange(3):
        koch(l, i)
        t.right(120)

def animated_insort(lst):
    turtle.tracer(0, 0) # disable turtle tracing
    pass

def run_tree():
    tree(80)
    turtle.done()

def run_koch():
    koch_all(300, 3)
    turtle.done()

def run_spirals():
    rec_spiral(180)
    t.up()
    t.goto(50, 50)
    t.down()
    it_spiral(100)
    turtle.done()

def run_list_show():
    lst = range(1, 10)
    random.shuffle(lst)
    draw_list(lst)
    turtle.done()

def run_animated_insort():
    #run_list_show()
    #lst = range(10, 1, -1)
    lst = [random.uniform(2, 15) for _ in xrange(18)]
    animated_insort(lst)

"""
    Sebi's turn.
"""

def sebiSpirals(nbspirals):
    pix = 10
    stop = nbspirals * 4

    while stop != 0:
        turtle.right(-90)
        turtle.forward(pix)
        pix += 5
        stop -= 1
    turtle.done()

#sebiSpirals(10)

def pulse():
    randlist = [random.randint(1,10) for i in range(10)]
    for i in randlist:
        turtle.towards(100,i)
        turtle.dot()
    turtle.done()




def squares(x,y,isFirst,levels, deviation):
    dev = 10    #position deviation for recursion

    #initial frame
    if isFirst:
        deviation += 1

        #turtle.setposition(x,y)
        turtle.setposition(x, y)
        for i in xrange(4):
            turtle.right(-90)
            turtle.forward(x*2)
        squares(x,y,0,2,0)

    # recursion
    elif levels>0:
        levels -= 1
        deviation += 1

        #upper left
        if 1:
            turtle.setposition(x+ dev * deviation , y - dev * deviation)

            for i in xrange(4):
                turtle.forward(abs(x//2**(deviation-1) + 2 * dev * deviation))
                turtle.right(90)

            squares(x,y,isFirst,levels,deviation)

        #upper right
        if 2:

            xr = yr = 0

            if x < 0 :
                xr =  x + abs(x)//2**(deviation-1) + dev * deviation
            else:
                xr = x + abs(x)//2**(deviation-1) + dev * deviation

            if y > 0:
                yr = y - dev * deviation
            else:
                yr = y + dev * deviation

            turtle.setposition(xr, yr)

            for i in xrange(4):
                turtle.forward(abs(x // 2 ** (deviation - 1) + 2 * dev * deviation))
                turtle.right(90)

            squares(x + abs(x), y, isFirst, levels, deviation)

        #down right
        if 3:
            xr3 = yr3 = 0

            if x < 0:
                xr3 = x + abs(x)//2**(deviation-1) + dev * deviation
            else:
                xr3 = x - abs(x)//2**(deviation-1) - dev * deviation

            if y > 0:
                yr3 = y - abs(y)//2**(deviation-1) - dev * deviation
            else:
                yr3 = y + abs(y)//2**(deviation-1) + dev * deviation

            turtle.setposition(xr3, yr3)

            for i in xrange(4):
                turtle.forward(abs(x // 2 ** (deviation - 1) + 2 * dev * deviation))
                turtle.right(90)

            squares(x+ abs(x), y - abs(y) , isFirst, levels, deviation)


squares(-200,200,1,2,0)