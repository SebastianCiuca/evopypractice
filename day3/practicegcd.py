#!/usr/bin/env python

def gcd(a,b):
	while b!=0:
		a,b = b, a % b
	return a

def test_gcd():
	assert gcd(1,2) == 1
	assert gcd(2,2) == 2
	assert gcd(2,4) == 2
	assert gcd(12,16) == 4
	assert gcd(25,125) == 25
	assert gcd(17,19) == 1

