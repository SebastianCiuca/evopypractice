import os
import json
import textwrap
import argparse
import sys
from validate import *
from datetime import datetime, timedelta

#from colors import *

global_id = 0

#data = datetime.datetime.now()

class Todo(object):
    def __init__(self, message, id=None, done=False, created=None):
        self.message = message
        self.id = id
        self.done = done
        self.created = created

def main():

    if len(sys.argv) == 1:
        print "Invalid parameters! \n Commands: {list,add,done}"
        return

    if (str(sys.argv[1]) == "add" or str(sys.argv[1]) == "done") and len(sys.argv) < 3:
        print "Invalid nb of parameters! 'add' requires a task and 'done' requires an id."
        return

    elif str(sys.argv[1]) == "list":
        command_list()

    elif str(sys.argv[1]) == "add":
        command_add(str(sys.argv[2]))

    elif str(sys.argv[1]) == "done":
        command_done(str(sys.argv[2]))



#encode
def to_json(obj1):

    obj = {}
    obj["message"] = obj1.message
    obj["id"] = str(obj1.id)
    obj["done"] = str(obj1.done)
    obj["created"] = isoformat(obj1.created)#unicode(obj1.created) isoformat

    return json.dumps(obj)

#decode
def from_json(linie_fisier):

    t = json.loads(linie_fisier)

    if t["done"] == "True":
        tobj = Todo(t["message"],t["id"],True,t["created"])
    else:
        tobj = Todo(t["message"],t["id"],False,t["created"])

    return tobj

def parse_file():

    tasks = []

    try:
        f = open("tasks.txt","r")

        for l in f:
            tasks.append(from_json(l)) #lista de obiecte Todo

        return tasks

    except IOError:
        raise IOError("Cannot open tasks file!")
    finally:
        f.close()

def populate_file(tasks):

    try:
        f = open("tasks.txt","w")

        #delete file content
        pfile.seek(0)
        pfile.truncate()

        #populate file
        for t in tasks:
            f.write(to_json(t) + "\n")

    except IOError:
        raise IOError("Cannot open tasks file!")
    finally:
        f.close()

def command_add(t):
    '''
    Add command
    '''

    #create new To do object
    newtask = Todo(t,0,False,datetime.now())

    #open file
    try:

        with open("tasks.txt", "a") as f:
            f.write(to_json(newtask) + "\n")

    except IOError:
        print "Cannot open tasks file!"
    finally:
        f.close()

def command_done(id):
    '''
    Done command
    '''

    try:
        id = validate_int(id)
    except ValueError as e:
        print e.strerror

    tasks = parse_file()

    for t in tasks:
        if t.id == id:
            t.done = True

    populate_file(tasks)

def command_list():
    '''
    List command
    '''
    try:
        tasks = parse_file()

        for t in tasks:
            if t.done == False:
                current_time = datetime.now()
                task_time = t.created
                #.0000%z // +0000 %Y
                #dif = datetime.strptime(str(current_time), '%Y-%m-%d %H:%M:%S%z') - datetime.strptime(str(task_time), '%a-%b-%d %H:%M:%S%z ')
                dif = datetime.strptime(str(current_time), "%Y-%m-%d %H:%M:%S.%f") - datetime.strptime(str(task_time), "%Y-%m-%d %H:%M:%S.%f")
                print "id " + str(t.id) + "\t to do \t "+ str(dif) +" ago \n\t" + str(t.message)

    except IOError as e:
        print e.strerror



main()