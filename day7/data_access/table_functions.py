import sqlite3

def get_table_rows(database, table,page_nb=None,per_page=None):
    """
    Returns a list of rows from a generic SELECT statement.
    """

    conn = sqlite3.connect(database)

    c = conn.cursor()

    c.execute('SELECT * FROM '+ str(table))



    item_list =  c.fetchall()

    conn.close()

    if page_nb == None and per_page == None:
        return item_list
    else:
        return item_list[page_nb*per_page:(page_nb+1)*per_page]


def get_condition(database, table, colname, value, op='='):
    """
    Returns a set of rows from a generic SELECT statement with condition.
    """

    conn = sqlite3.connect(database)

    c = conn.cursor()

    c.execute('SELECT * FROM '+ str(table)+ ' WHERE ' + str(colname) + str(op) + str(value))

    item_list =  c.fetchall()

    conn.close()

    return item_list