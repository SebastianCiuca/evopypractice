def milisec_to_min(mili):
    """
    Converts an int (representing miliseconds) to an int representing minutes.
    """
    return int(mili // (6 * 10**4))


def min_to_milisec(mins):
    """
    Converts an int (representing minutes) to an int representing miliseconds.
    """
    return int(mins * (6 * 10**4))


def get_max_namelength(artist_list):
    """
    Returns an int = maximum length of a string in the given list of touples.
    """
    max = 0

    for a in artist_list:
        if len(a[1]) > max:
            max = len(a[1])

    return max

def get_max_namelength2(artist_list):
    """
    Returns an int = maximum length of a string in the given list.
    """
    max = 0

    for a in artist_list:
        if len(a) > max:
            max = len(a)

    return max


def persist_csv(result_list,file_name):
    """
    Writes the content of a list of touples in a file, CSV format.
    """

    with open(file_name,"w") as f:

        for t in result_list:
            to_write = str(t[0]) + "," + str(t[1]) + "," + str(t[2]) + "," + str(t[3]) + "\n"
            f.write(to_write)


def format_print(length,item=None):
    """
     Prints an item in a specific format.
     (for artists)
    """

    print "+" + (length + 10) * "-" + "+"

    if item!=None:
        padding = (length - len(item)) // 2
        print "|\t" + " " * padding + item + " " * padding + "\t|"