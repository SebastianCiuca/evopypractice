#!/usr/bin/python

import sqlite3
import csv
from table_functions import *
from utils import *

def list_artists(artist_list):

    maxlen = get_max_namelength(artist_list)

    for a in artist_list:
        format_print(maxlen,a[1])

    format_print(maxlen)


def main():
    list_artists(get_table_rows("chinook.db","artists",4,12))

#main()


"""
afiseaza artistii care au melodii mai lungi de 10 minute
"""

def long_tracks(mins):

    tracks = get_condition("chinook.db","tracks","Milliseconds",str(min_to_milisec(mins)),'>')

    info = []

    for t in tracks:
        album = get_condition("chinook.db","albums","AlbumId",int(t[2]))

        artist = get_condition("chinook.db","artists","ArtistId",int(album[0][2]))

        info.append((artist[0][1],t[1],album[0][1],milisec_to_min(t[6])))

    artists = [t[0] for t in info]

    ret = []

    for t in info:
        if t[0] in artists:
            ret.append(t)
        artists = [a for a in artists if a!=t[0]]
    return ret

def print_track(artistlen,tracklen,albumlen,item_touple=None):
    print "+" + (artistlen) * "-" + "+",
    print (tracklen) * "-" + "+",
    print (albumlen) * "-" + "+",
    print 10 * "-" + "+"

    if item_touple != None:
        padding1 = (artistlen - len(item_touple[0])) // 2
        padding2 = (tracklen - len(item_touple[1])) // 2
        padding3 = (albumlen - len(item_touple[2])) // 2
        padding4 = 4

        print "|" + " " * padding1 + item_touple[0] + " " * padding1 + "|",
        print " " * padding2 + item_touple[1] + " " * padding2 + "|",
        print " " * padding3 + item_touple[2] + " " * padding3 + "|",
        print " " * padding4 + str(item_touple[3]) + " " * padding4 + "|"

def print_track2(artistlen,tracklen,albumlen,item_touple=None):

    # {| {:^30}|{}|{}|{} }.format(t[0],t[1],t[2],[3],"|")

    # print "+" + (artistlen) * "-" + "+",
    # print (tracklen) * "-" + "+",
    # print (albumlen) * "-" + "+",
    # print 10 * "-" + "+"

    #print "+ {:} | {:} | {:} | {:} +".format(artistlen,tracklen,albumlen,2)

    pass


def list_tracks(tracks_list):

    mylist = [t[0] for t in tracks_list]
    maxlen_artist = get_max_namelength2(mylist)

    mylist = [t[1] for t in tracks_list]
    maxlen_track = get_max_namelength2(mylist)

    mylist = [t[2] for t in tracks_list]
    maxlen_album = get_max_namelength2(mylist)


    print str(maxlen_album)

    for t in tracks_list:
        print_track(maxlen_artist,maxlen_album,maxlen_track,t)

    print_track(maxlen_artist, maxlen_album, maxlen_track)

def main2():
    persist_csv(long_tracks(10),"tracks.csv")

#main2()
print_track2(10,10,10)

main2()