"""
1) primele doua stiri despre DNA de pe hotnews.ro .
"""

from bs4 import BeautifulSoup
from urllib2 import urlopen

#find("tag","{ "attr" : "value"}") #ex: attr = id, value = articleContent

#find("a","{ id : href }")

def main():

    articles = find_links("http://www.hotnews.ro/","DNA",2)

    to_print = get_articles(articles)

    print_list(to_print)




def find_links(page_url,interest,count):
    file = urlopen(page_url)
    soup = BeautifulSoup(file,"html.parser")

    body = soup.body

    dna_links = []

    for a in body.find_all("a"):
        if 'href' in a.attrs.keys():
            found = len([i for i in [interest, interest.upper(), interest.lower()] if i in a.attrs["href"]])

            if found > 0:
                dna_links.append(a.attrs['href'])

            if len(dna_links) == count:
                break

    return dna_links





def get_articles(link_lists):

    articles = []

    for link in set(link_lists):
        link_page = urlopen(link)
        soup2 = BeautifulSoup(link_page,"html.parser")

        body2 = soup2.body

        for el in body2.find_all("div"):
            if "id" in el.attrs.keys():
                if "articleContent" in el.attrs["id"]:
                    articles.append(el.getText())

    return articles


def print_list(alist):

    for el in alist:
        print el



main()