
"""
1 2 3
2 9 10
8 18 21
=> se poate elimina doar 1/4 din spatiu
"""

def div_et_imp(alist,el):

    l = 0
    r = len(alist)-1

    while l<=r:
        m =(l+r)//2

        if el > alist[m]:
            l = m + 1
        elif el< alist[m]:
            r = m - 1
        else:
            return True
    return False

def matrix_divide_impera(amat,el):

    for l in amat:

        if div_et_imp(l,el):
            return True
    return False

#print div_et_imp([1,2,7,9,10,123],11)
print matrix_divide_impera([[1,2,3],[4,9,8],[2,7,98]],6)
