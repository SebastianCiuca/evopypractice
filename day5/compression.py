
def compress(archive):
    """
    >>> compress([0,0,1])
     [0,2,1,1]
     [(0,2),(1,1)]
    """
    if archive == None or archive == []:
        return []

    res = []

    aux = archive[0]
    seq_len = 1

    for item in archive[1:]:

        if item == aux:
            seq_len += 1
        else:
            #res.append((aux,seq_len))
            res.extend([aux,seq_len])
            aux = item
            seq_len = 1

    #res.append((aux, seq_len))
    res.extend([aux, seq_len])

    return res

print compress([0,0,0,1,1,7,0,0])


def decompress(comp):

    if comp == None or comp == []:
        return []

    res = []

    for i in xrange(0,len(comp),2):
        res += [comp[i]]*comp[i+1]

    return res

print decompress([0, 3, 1, 2, 7, 1, 0, 2])


def test_compress():
    #assert compress([0,0,0,1,1,7,0,0]) == [(0,3),(1,2),(7,1),(0,2)]
    assert compress([0,0,0,1,1,7,0,0]) == [0,3,1,2,7,1,0,2]
    assert compress("ana") == ['a',2,'n',1]
    assert compress([]) == []

def stest_decompress():
    assert decompress([(0, 3), (1, 2), (7, 1), (0, 2)]) == [0,0,0,1,1,7,0,0]
    assert decompress([]) == []